# Using C++ in VS Code using Chocolatey

## Steps

1. [Install MinGW using chocolatey](https://chocolatey.org/packages/mingw).
2. Copy contents of `.vscode` folder into working directory.
3. Press `F5` to debug or `ctrl-F5` to compile and run without debugging.
4. ???
5. Profit.

## Disclaimer

These instructions are more for me so I don't have to reinvent the wheel anytime I reset my PC. So, don't expect any help from me when this doesn't work for you ♥